DROP DATABASE IF EXISTS docker;

DROP ROLE IF EXISTS docker;
CREATE ROLE docker LOGIN PASSWORD 'docker';

CREATE DATABASE docker;

GRANT ALL PRIVILEGES ON DATABASE docker TO docker;

\c docker

CREATE EXTENSION postgis;

DROP TABLE IF EXISTS users;
CREATE TABLE users(
id SERIAL,
username VARCHAR(32),
password_hash VARCHAR(128),
CONSTRAINT "PKey" PRIMARY KEY ("id", "username")
);

DROP TABLE IF EXISTS postal_codes;
CREATE TABLE postal_codes(
the_geom geometry(Geometry,4326),
code INTEGER,
id INTEGER PRIMARY KEY
);

DROP TABLE IF EXISTS paystats;
CREATE TABLE paystats(
amount DOUBLE PRECISION,
p_month DATE,
p_age VARCHAR,
p_gender CHAR(1),
postal_code_id INTEGER,  -- REFERENCES postal_codes(id),
id INTEGER PRIMARY KEY
);

-- index
DROP INDEX IF EXISTS postal_codes_idx;
CREATE INDEX postal_codes_idx
  ON postal_codes
  (code);

DROP INDEX IF EXISTS paystats_postal_code_idx;
CREATE INDEX paystats_postal_codes_idx
  ON paystats
  (postal_code_id);

COPY paystats FROM '/app/data/paystats.csv' DELIMITER ',' CSV HEADER;
COPY postal_codes FROM '/app/data/postal_codes.csv' DELIMITER ',' CSV HEADER;

ALTER TABLE postal_codes ALTER COLUMN the_geom TYPE geometry(MultiPolygon,4326) USING ST_Multi(the_geom);

DROP INDEX IF EXISTS postal_codes_geom_idx;
CREATE INDEX postal_codes_geom_idx
  ON postal_codes
  USING GIST
  (the_geom);

CREATE OR REPLACE VIEW postal_code_paystats_json_view AS
SELECT pcodes.code, json_build_object('type',pcodes.type,'geometry',pcodes.geometry,'properties',pcodes.properties) as pcs
FROM(
  SELECT pc.code, 'Feature' AS type, ST_AsGeoJSON(pc.the_geom)::json AS geometry,
    (SELECT json_strip_nulls(json_agg(ps))
        FROM (
            SELECT * FROM paystats WHERE postal_code_id = pc.id
        ) ps
    ) AS properties
  FROM postal_codes AS pc) pcodes;


CREATE OR REPLACE VIEW postal_code_json_view AS
SELECT pcodes.code, json_build_object('type',pcodes.type,'geometry',pcodes.geometry,'properties',pcodes.properties) as pcs
FROM(
  SELECT pc.code, 'Feature' AS type, ST_AsGeoJSON(pc.the_geom)::json AS geometry, json_build_object('code', pc.code) as properties
  FROM postal_codes AS pc) pcodes;


CREATE OR REPLACE VIEW postal_code_paystats_aggregated_json_view AS
SELECT pcodes.code, json_build_object('type',pcodes.type,'geometry',pcodes.geometry,'properties',pcodes.properties) as pcs
FROM(
  SELECT pc.code, 'Feature' AS type, ST_AsGeoJSON(pc.the_geom)::json AS geometry,
    (SELECT json_strip_nulls(json_agg(ps))
        FROM (
            select pc.code as code, p_age, p_gender, sum(amount) as amount from paystats where postal_code_id = pc.id group by pc.code, p_age, p_gender order by pc.code asc, p_age desc, p_gender
        ) ps
    ) AS properties
  FROM postal_codes AS pc) pcodes;


GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO docker;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO docker;


