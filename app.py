from flask import Flask, request, jsonify, g, abort, url_for
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from geoalchemy2 import Geometry

from flask_httpauth import HTTPBasicAuth
from passlib.apps import custom_app_context as pwd_context
from itsdangerous import (TimedJSONWebSignatureSerializer as Serializer, BadSignature, SignatureExpired)

from flask_caching import Cache

app = Flask(__name__)

cache = Cache(app, config={'CACHE_TYPE': 'simple'})

auth = HTTPBasicAuth()

app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://docker:docker@127.0.0.1/docker'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = 'pbkdf2_sha512'
db = SQLAlchemy(app)
ma = Marshmallow(app)


class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(32), index=True)
    password_hash = db.Column(db.String(128))

    def hash_password(self, password):
        self.password_hash = pwd_context.encrypt(password)

    def verify_password(self, password):
        return pwd_context.verify(password, self.password_hash)

    def generate_auth_token(self, expiration=600):
        s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
        return s.dumps({'id': self.id})

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None  # valid token, but expired
        except BadSignature:
            return None  # invalid token
        user = User.query.get(data['id'])
        return user


@app.route('/api/users', methods=['POST'])
def new_user():
    username = request.json.get('username')
    password = request.json.get('password')
    if username is None or password is None:
        abort(400)  # missing arguments
    if User.query.filter_by(username=username).first() is not None:
        abort(400)  # existing user
    user = User(username=username)
    user.hash_password(password)
    db.session.add(user)
    db.session.commit()
    return jsonify({'username': user.username}), 201, {'Location': url_for('new_user', id=user.id, _external=True)}


@app.route('/api/token')
@auth.login_required
def get_auth_token():
    token = g.user.generate_auth_token()
    return jsonify({'token': token.decode('ascii')})


@auth.verify_password
def verify_password(username_or_token, password):
    # first try to authenticate by token
    user = User.verify_auth_token(username_or_token)
    if not user:
        # try to authenticate with username/password
        user = User.query.filter_by(username=username_or_token).first()
        if not user or not user.verify_password(password):
            return False
    g.user = user
    return True


class Paystats(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    amount = db.Column(db.String(80), unique=True)
    p_month = db.Column(db.String(120), unique=False)
    p_age = db.Column(db.String(120), unique=False)
    p_gender = db.Column(db.String(1), unique=False)
    postal_code_id = db.Column(db.Integer)

    def __init__(self, id, amount, p_month, p_age, p_gender, postal_code_id):
        self.id = id
        self.amount = amount
        self.p_month = p_month
        self.p_age = p_age
        self.p_gender = p_gender
        self.postal_code_id = postal_code_id


class PaystatsSchema(ma.Schema):
    class Meta:
        # Fields to expose
        fields = ('amount', 'p_month', 'p_age', 'p_age', 'p_gender', 'postal_code_id')


class PostalCodes(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    the_geom = db.Column(Geometry(srid=4326))
    code = db.Column(db.Integer)

    def __init__(self, id, the_geom, code):
        self.id = id
        self.the_geom = the_geom
        self.code = code


class PostalCodesSchema(ma.Schema):
    class Meta:
        # Fields to expose
        fields = ('the_geom', 'code')


class PostalCodesPaystatsJSON(db.Model):
    __tablename__ = 'postal_code_paystats_json_view'
    code = db.Column(db.Integer, primary_key=True)
    pcs = db.Column(db.JSON)


class PostalCodesPaystatsJSONSchema(ma.Schema):
    __tablename__ = 'postal_code_paystats_json_view'

    class Meta:
        # Fields to expose
        fields = ('code', 'pcs')

class PostalCodesJSON(db.Model):
    __tablename__ = 'postal_code_json_view'
    code = db.Column(db.Integer, primary_key=True)
    pcs = db.Column(db.JSON)


class PostalCodesJSONSchema(ma.Schema):
    __tablename__ = 'postal_code_json_view'

    class Meta:
        # Fields to expose
        fields = ('code', 'pcs')

class PostalCodesPaystatsAggregatedJSON(db.Model):
    __tablename__ = 'postal_code_paystats_aggregated_json_view'
    code = db.Column(db.Integer, primary_key=True)
    pcs = db.Column(db.JSON)


class PostalCodesPaystatsAggregatedJSONSchema(ma.Schema):
    __tablename__ = 'postal_code_paystats_aggregated_json_view'

    class Meta:
        # Fields to expose
        fields = ('code', 'pcs')


paystats_schema = PaystatsSchema()
paystatsm_schema = PaystatsSchema(many=True)

postal_code_schema = PostalCodesSchema()
postal_codes_schema = PostalCodesSchema(many=True)

postal_code_paystats_json_schema = PostalCodesPaystatsJSONSchema()
postal_codes_paystats_json_schema = PostalCodesPaystatsJSONSchema(many=True)

postal_code_json_schema = PostalCodesJSONSchema()
postal_codes_json_schema = PostalCodesJSONSchema(many=True)

postal_code_paystats_aggregated_json_schema = PostalCodesPaystatsAggregatedJSONSchema()
postal_codes_paystats_aggregated_json_schema = PostalCodesPaystatsAggregatedJSONSchema(many=True)


@app.route("/paystats", methods=["GET"])
@cache.cached(timeout=50)
@auth.login_required
def get_paystats():
    d = {"type": "FeatureCollection", "features": []}
    all_paystats = PostalCodesPaystatsJSON.query.all()
    for n, i in enumerate(all_paystats):
        i.pcs.update({'id': i.code})
        d["features"].append(i.pcs)
    return jsonify(d)


@app.route("/paystats/<id>", methods=["GET"])
@cache.cached(timeout=50)
@auth.login_required
def paystats_detail(id):
    pcpj = PostalCodesPaystatsJSON.query.get(id)
    return jsonify(pcpj.pcs)


@app.route("/postalcodes", methods=["GET"])
@cache.cached(timeout=50)
@auth.login_required
def get_postalcode():
    d = {"type": "FeatureCollection", "features": []}
    all_paystats = PostalCodesJSON.query.all()
    for n, i in enumerate(all_paystats):
        i.pcs.update({'id': i.code})
        d["features"].append(i.pcs)
    return jsonify(d)


@app.route("/postalcodes/<id>", methods=["GET"])
@cache.cached(timeout=50)
@auth.login_required
def postalcode_detail(id):
    pcpj = PostalCodesJSON.query.get(id)
    return jsonify(pcpj.pcs)


@app.route("/paystats/aggregated", methods=["GET"])
@cache.cached(timeout=50)
@auth.login_required
def paystats_aggregated():
    d = {"type": "FeatureCollection", "features": []}
    all_paystats = PostalCodesPaystatsAggregatedJSON.query.all()
    for n, i in enumerate(all_paystats):
        i.pcs.update({'id': i.code})
        d["features"].append(i.pcs)
    return jsonify(d)


@app.route("/paystats/aggregated/<id>", methods=["GET"])
@cache.cached(timeout=50)
@auth.login_required
def paystats_detail_aggregated(id):
    pcpj = PostalCodesPaystatsAggregatedJSON.query.get(id)
    return jsonify(pcpj.pcs)


if __name__ == '__main__':
    app.run(debug=True)
