FROM geographica/postgis:pleasant_yacare
RUN apt-get update -y
RUN apt-get install -y python3 python-pip python-dev build-essential
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt
# Aquí se debería arrancar posgres pero no funciona. Si por esto fuere andaría
RUN /etc/init.d/postgresql start && /usr/bin/psql -a < /app/data/database.sql
#ENTRYPOINT ["python"]
#CMD ["app.py"]
CMD python3 /app/app.py
EXPOSE 5000
